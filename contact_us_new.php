<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Hostel</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <!-- Your custom styles (optional) -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- main Js -->
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>
    

    <!-- Start your project here-->
        
    <?php include 'header.php'; ?>
    <main>
        <div class="container">
            <div id="headTitle">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-xs-center">Contact Us</h2>
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-6">
                    <h4>FILL IN THIS FORM</h4>
                    <p>An associate from Butik Capsule Hostel will be in touch within 24 hours</p>

                    <h4>General</h4>
                    <br>
                    <h5>We are at</h5>
                    <p>Jl WR.Supratman 2L Malang, Jawa Timur, Indonesia</p>

                    <h5>Call us at</h5>
                    <p>+ 62341 - 368278</p>
                    <p>+ 6281 336 851 515</p>
                    <p>+ 6287 812 442 888</p>

                    <h5>Email us at</h5>
                    <p>info@butikcapsulehotel.com</p>
                    <p>* Check in call after 10.00 pm</p>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <form class="col s12">
                          <div class="row">
                            <div class="input-field col s12">
                              <input id="name" type="text" class="validate">
                              <label for="name">Name</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                              <input id="email" type="email" class="validate">
                              <label for="email" data-error="Please Enter Valid Email" data-success="">Email</label>
                            </div>
                          </div>
                          <div class="row">
                            <div class="input-field col s12">
                              <input id="icon_prefix" type="text" class="validate">
                              <label for="icon_prefix">Subject</label>
                            </div>
                          </div>
                          <div class="row">
                                <div class="input-field col s12">
                                  <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                                  <label for="icon_prefix2">Messages</label>
                                </div>
                          </div>
                            <button class="btn waves-effect waves-light pull-right" type="submit" name="action" id="mt-10" >Submit
                                <i class="material-icons right">send</i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <?php include 'footer.php'; ?> 

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <!-- Materialize core Javascript -->
    <script type="text/javascript" src="js/materialize.js"></script>

    <!-- main Js -->
    <script src="text/javascript" src="js/main.js"></script>


</body>

</html>