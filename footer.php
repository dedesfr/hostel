<!--Footer-->
<footer class="page-footer info-color center-on-small-only">

    <!--Footer Links-->
    <div class="container">
        <div class="row">

            <div class="col-md-6 text-xs-center">
                <p>Call +62 341 368 278</p>
            </div>

            <div class="col-md-6 text-xs-center">
                <p>F.A.Q</p>
            </div>
        </div>
    </div>
    <!--/.Footer Links-->

    <!--Copyright-->
    <div class="footer-copyright">
        <div class="container-fluid">
            © 2016 Copyright: <a href="#"> Hostel </a>

        </div>
    </div>
    <!--/.Copyright-->

</footer>
<!--/.Footer-->