<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Hostel</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <!-- Your custom styles (optional) -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- main Js -->
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>
    
    <?php include 'header.php'; ?>
    <br>
    <main>
        <div class="container">
            <div id="first_section">
                <div id="headTitle">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-xs-center">What You Get</h2>
                        </div>
                    </div>
                </div>
                <hr id="icon_div">
                <div id="icon_section">
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fa fa-wifi fa-4x"></i>
                            <p class="text-xs-center">FREE WIFI</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-cutlery fa-4x"></i>
                            <p>FREE BREAKFAST</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-laptop fa-4x"></i>
                            <p>EQUIPPED UTILITY ROOM</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-beer fa-4x"></i>
                            <p>FREE BOOTLED MINERAL WATER</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-wifi fa-4x"></i>
                            <p>FREE TOWELS</p>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-wifi fa-4x"></i>
                            <p>FREE SOAP & SHAMPOO</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div id="second_section">
                <h3>PAY FOR THE STAY</h3>
                <ul class="list-unstyled">
                    <li>24-hour CCTV</li>
                    <li>Secure keycard access</li>
                    <li>Female-only CAPSULEs</li>
                    <li>Self-contained bathrooms</li>
                    <li>Self-service pantry</li>
                    <li>Personal locker under CAPSULEs</li>
                </ul>

                <div class="row">
                    <div class="col-md-3">
                        <div class="view overlay hm-white-light z-depth-1-half wyg_img">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="view overlay hm-white-light z-depth-1-half wyg_img">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="view overlay hm-white-light z-depth-1-half wyg_img">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="view overlay hm-white-light z-depth-1-half wyg_img">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                </div>

                <h3>Complimentary Details</h3>
                <p>Each CAPSULE is equipped with: </p>
                <ul class="list-unstyled">
                    <li>1 pillow + 1 blanket + sheets</li>
                    <li>Personal locker under CAPSULE (53 x 38 x 34 cm)</li>
                    <li>2 power sockets + reading light</li>
                    <li>Clothes rack + hangers</li>
                </ul>
                <p>Bathroom Amenities: Towels, shampoo, shower gel, tooth brush and hairdryer</p>
            </div>


        </div>
    </main>


    <?php include 'footer.php'; ?> 

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <!-- Materialize core Javascript -->
    <script type="text/javascript" src="js/materialize.js"></script>

    <!-- main Js -->
    <script src="text/javascript" src="js/main.js"></script>


</body>

</html>