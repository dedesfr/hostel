<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Hostel</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <!-- Your custom styles (optional) -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- main Js -->
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>
    

    <!-- Start your project here-->
        
    <?php include 'header.php'; ?>
    <br>
    <main>

        <!--Main layout-->
        <div class="container">
            <!--First row-->
            <div id="headTitle">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-xs-center">Just eat, travel, sleep and repeat</h2>
                    </div>
                    <div class="col-md-12">
                        <h2 class="text-xs-center">we are here for you</h2>
                    </div>
                    <div class="col-md-12">
                        <p class="text-xs-center">Butik Capsule Hostel, a clean, futuristic compact hostel in the city center of malang, East Java.</p>
                    </div>
                </div>
            </div>
            <!--/.First row-->

            <!--Second row-->
            <div class="img-content">
                <div class="row">
                    <div class="col-md-4">
                    <a href="contact_us.php">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </a>
                    </div>
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="section_2">
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" id="section_3">
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="view overlay hm-white-light z-depth-1-half imeg">
                            <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                            <h2 class="text-xs-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, placeat!</h2>
                            <div class="mask">
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div id="book">
                    <h3 class="text-xs-center">Book Now</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <a href="https://www.traveloka.com/hotel/indonesia/butik-capsule-hostel-3000010008262?id=1936398747104117762&kw=1936398747104117762_nokeyword&gn=g&gd=c&gdm=&gcid=147615957259&gdp=&gdt=&gap=1t2&pc=1&cp=1936398747104117762_HDS-LOC-D-s_1936398747104117762_MLG&kid=f9250ad3-b8b3-4e1a-97d0-1ee4db489387&aid=21260230035&wid=dsa-234119864955&fid=&gid=1007716&adloc=id-id&gclid=CILnp-OG4tACFQqhaAodNasIwA" target="_blank">
                                <img src="images/traveloka.png">
                                <p class="text-xs-center">Traveloka.com</p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="http://www.booking.com/hotel/id/butik-capsule-hostel.id.html" target="_blank">
                                <img src="images/agoda.png">
                                <p class="text-xs-center">Agoda.com</p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="https://www.agoda.com/id-id/butik-capsule-hostel/hotel/malang-id.html" target="_blank">
                                <img src="images/booking_com.png">
                                <p class="text-xs-center">Booking.com</p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="https://www.tripadvisor.co.id/Hotel_Review-g297710-d8685922-Reviews-Butik_Capsule_Hostel-Malang_East_Java_Java.html" target="_blank">
                                <img src="images/tripadvisor.png">
                                <p class="text-xs-center">Tripadvisor.com</p>
                            </a>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <div id="reception" class="text-xs-center blue lighten-5">
            <div class="wrapper">
                <p>Reception Operating Hours</p>
                <p>Monday - Sunday</p>
                <p>6am - 10pm</p>
            </div>
        </div>
        <div id="get_in_touch" class="text-xs-center">
            <p>GET IN TOUCH</p>
            <div class="wrapper">
                <p>+62 341 368 278</p>
                <p>+6281 336 851 515</p>
                <p>info@butikcapsulehostel.com</p>
                
                <div id="get_in_touch_address">
                    <p>Jl WR.Supratman 2L </p>
                    <p>Malang, Jawa Timur </p>
                    <p>Indonesia</p>
                </div>
                    <i class="fa fa-facebook-official fa-2x"></i>
                    <i class="fa fa-instagram fa-2x"></i>
                    
                </div>
            </div>
        </div>

    </main>
    <?php include 'footer.php'; ?>



    <!-- /Start your project here-->
    

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <!-- Materialize core Javascript -->
    <script type="text/javascript" src="js/materialize.js"></script>

    <!--Google Maps-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU_Ocfdfjv0-Hq3rUgO7d14pG87aw8-YE&callback=initMap"
    async defer></script>

    <!-- main Js -->
    <script src="text/javascript" src="js/main.js"></script>

    <script>
        function init_map() {

            var var_location = new google.maps.LatLng(-6.188110, 106.738594);

            var var_mapoptions = {
                center: var_location,

                zoom: 14
            };

            var var_marker = new google.maps.Marker({
                position: var_location,
                map: var_map,
                title: "New York"
            });

            var var_map = new google.maps.Map(document.getElementById("map-container"),
                var_mapoptions);

            var_marker.setMap(var_map);

        }

        google.maps.event.addDomListener(window, 'load', init_map);
    </script>


</body>

</html>

