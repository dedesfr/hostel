<header>

    <!--Navbar-->
      <nav class="navbar navbar-dark bg-primary white">
        <div class="nav-wrapper container">
            <a href="index.php" class="brand-logo">Logo</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="what_you_get.php">Our Services</a></li>
                <li><a href="mobile.html">Neighbourhood</a></li>
                <li><a href="mobile.html">Tours</a></li>
                <li><a href="badges.html">About Us</a></li>
                <li><a href="badges.html">Book Now</a></li>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="what_you_get.php">Our Services</a></li>
                <li><a href="mobile.html">Neighbourhood</a></li>
                <li><a href="mobile.html">Tours</a></li>
                <li><a href="badges.html">About Us</a></li>
                <li><a href="badges.html">Book Now</a></li>
            </ul>
        </div>
      </nav>
    <!--/.Navbar-->

</header>