<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Hostel</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <!-- Your custom styles (optional) -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- main Js -->
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>
    

    <!-- Start your project here-->
        
    <?php include 'header.php'; ?>
    <br>
    <main>
        <div class="container">
            <div id="headTitle">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-xs-center">About Us</h2>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div id="about_us_first_section">
            <div class="row">
                <div class="col-md-4">
                    <h5>We are at</h5>
                    <p>Jl WR.Supratman 2L Malang, Jawa Timur, Indonesia</p>

                    <h5>Call us at</h5>
                    <ul>
                        <li>+ 62341 - 368278</li>
                        <li>+ 6281 336 851 515</li>
                        <li>+ 6287 812 442 888</li>
                    </ul>

                    <h5>Email us at</h5>
                    <ul>
                        <li>info@butikcapsulehotel.com</li>
                        <li>* Check in call after 10.00 pm</li>
                    </ul>

                </div>
                <div class="col-md-8">
                    <p>Hello this is Butik Capsule hostel</p>
                    <p>We are Malang's first capsule hotel. specialy designed for travellers who seek simple, convenient and affordable stay.</p>
                    <p>With 32 cozy. minimalistic capsules located at the central bussiness and entertainment district of Malang. You can look forward to convenience at your fingertips without compromising on comfort, quality or style</p>
                    <p>We also provide free complimentarty food and beverages, bath essentials and Wi-Fi access, along with personalized concierge services on hand to satisfy  your every need so that you can focus on the things that truly matter.</p>
                </div>

            </div>
            </div>
            <div id="about_us_img">
            <div class="col-md-6">
                <h3>SIDE ENTRY SINGLE CAPSULE</h3>
                <ul>
                    <li>Fits one comfortably</li>
                    <li>a pillow</li>
                    <li>Personal locker under CAPSULE</li>
                </ul>
            </div>
            <div class="col-md-6">
                <div class="view overlay hm-white-light z-depth-1-half wyg_img">
                    <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                    <div class="mask">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="view overlay hm-white-light z-depth-1-half wyg_img">
                    <img src="http://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg">
                    <div class="mask">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h3>SIDE ENTRY SINGLE CAPSULE</h3>
                <ul>
                    <li>Fits one comfortably</li>
                    <li>a pillow</li>
                    <li>Personal locker under CAPSULE</li>
                </ul>
            </div>
            </div>
        </div>

        <div class="container">
            <div id="detail_section">
                <h2>Details</h2>
                <hr>
                <h5>Locker Dimensions</h5>
                <ul>
                    <li>H: 34 cm</li>
                    <li>W: 38 cm</li>
                    <li>L: 53 cm</li>
                </ul>
                <ul>
                    <li>CHECK IN/OUT</li>
                    <li>Check-in: 2pm-10pm (Please call us if after 10pm)</li>
                    <li>Check-out: 12pm</li>
                </ul>
                <p>Reception operating hours: 6am-10pm</p>

                <h5>Each CAPSULE comes equipped with: </h5>
                <div id="equipped">
                    <ul>
                        <li>Personal reading light</li>
                        <li>Luxurious cotton sheets</li>
                        <li>Fluffy blanket</li>
                        <li>Hangers and a clothes rack</li>
                        <li>Personal power socket</li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
    <?php include 'footer.php'; ?>



    <!-- /Start your project here-->
    

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <!-- Materialize core Javascript -->
    <script type="text/javascript" src="js/materialize.js"></script>

    <!--Google Maps-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU_Ocfdfjv0-Hq3rUgO7d14pG87aw8-YE&callback=initMap"
    async defer></script>

    <!-- main Js -->
    <script src="text/javascript" src="js/main.js"></script>


</body>

</html>

