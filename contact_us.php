<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>Hostel</title>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <!-- Your custom styles (optional) -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- main Js -->
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>
    

    <!-- Start your project here-->
        
    <?php include 'header.php'; ?>
    <main>
        <!-- Purple Header -->
        <div class="edge-header info-color"></div>

        <!-- Main Container -->
        <div class="container free-bird">
            <div class="container" id="form_contact">
                <div class="row">
                    <div class="col-md-8 col-lg-8 mx-auto white z-depth-1 py-2 px-2">
                        <div class="row">
                            <h2>Contact Us</h2>
                            <form class="col s12" id="form_contact_us">
                              <div class="row">
                                <div class="input-field col s6">
                                  <i class="material-icons prefix">account_circle</i>
                                  <input id="icon_prefix" type="text" class="validate">
                                  <label for="icon_prefix">Name</label>
                                </div>
                                <div class="input-field col s6">
                                  <i class="material-icons prefix">email</i>
                                  <input id="email" type="email" class="validate">
                                  <label for="email" data-error="Please Enter Valid Email" data-success="">Email</label>
                                </div>
                                <div class="input-field col s6">
                                  <i class="material-icons prefix">subject</i>
                                  <input id="icon_prefix" type="text" class="validate">
                                  <label for="icon_prefix">Subject</label>
                                </div>
                              </div>
                              <div class="input-field col s12">
                                  <i class="material-icons prefix">mode_edit</i>
                                  <textarea id="icon_prefix2" class="materialize-textarea"></textarea>
                                  <label for="icon_prefix2">Messages</label>
                             </div>
                                <button class="btn waves-effect waves-light" type="submit" name="action" id="mt-10" >Submit
                                    <i class="material-icons right">send</i>
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 mx-auto white z-depth-1 py-2 px-2 mdb-color darken-1">
                        <div class="container" id="contact_info">
                            <div class="row">
                                <h2>Contact Information</h2>
                                <div class="row">
                                <div class="col-md-12">
                                    <ul class="text-xs-center">
                                        <li id="contact_info_li"><i class="fa fa-map-marker fa-2x"></i>
                                            <p>Jl WR.Supratman 2L Malang, Jawa Timur, Indonesia</p>
                                        </li>

                                        <li id="contact_info_li"><i class="fa fa-phone fa-2x"></i>
                                            <p>+ 62341 - 368278</p>
                                            <p>+ 6281 336 851 515</p>
                                            <p>+ 6287 812 442 888</p>
                                        </li>

                                        <li><i class="fa fa-envelope fa-2x"></i>
                                            <p>info@butikcapsulehotel.com</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.Main Container -->

    </main>
    <?php include 'footer.php'; ?> 

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/tether.min.js"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <!-- Materialize core Javascript -->
    <script type="text/javascript" src="js/materialize.js"></script>

    <!-- main Js -->
    <script src="text/javascript" src="js/main.js"></script>


</body>

</html>